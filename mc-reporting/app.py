from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps
from mysqlhelper import *

app = Flask(__name__)

@app.route("/reporting", methods=['GET'])
def getbarang():
    my = MysqlHelper()
    try:
        result = my.get("reporting")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/reporting/<int:id>", methods=['GET'])
def getbarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.get_where("reporting", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/reporting", methods=['POST'])
def insertbarang():
    my = MysqlHelper()
    try:
        data = request.json
        if "date" in data and "report_number" in data and "maker" in data and "description" in data:
            data = { 
                "date": str(data['date']),
                "report_number": str(data['report_number']),
                "maker": str(data['maker']),
                "description": str(data['description'])
            }
            result = my.insert("reporting", data)
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"date, report_number, maker, description", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/reporting/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    my = MysqlHelper()
    try:
        data = request.json
        if "date" in data and "report_number" in data and "maker" in data and "description" in data:
            data = {
                "date": str(data['date']),
                "report_number": str(data['report_number']),
                "maker": str(data['maker']),
                "description": str(data['description'])
            }
            result = my.update("reporting", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
             return jsonify({"data":"date, report_number, maker, description", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/reporting/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.delete("reporting", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    # app.run(debug=True, port=1000)
    app.run(debug=True, host="0.0.0.0", port=5000)
