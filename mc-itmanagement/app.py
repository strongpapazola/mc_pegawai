from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps
from mysqlhelper import *

app = Flask(__name__)

@app.route("/it_management", methods=['GET'])
def getbarang():
    my = MysqlHelper()
    try:
        result = my.get("it_management")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/it_management/<int:id>", methods=['GET'])
def getbarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.get_where("it_management", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/it_management", methods=['POST'])
def insertbarang():
    my = MysqlHelper()
    try:
        data = request.json
        if "request_type" in data and "asset_id" in data and "employee_id" in data and "description" in data:
            data = { 
                "request_type": str(data['request_type']),
                "asset_id": int(data['asset_id']),
                "description": str(data['description']),
                "employee_id": int(data['employee_id'])
            }
            result = my.insert("it_management", data)
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"request_type, asset_id, description, employee id", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/it_management/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    my = MysqlHelper()
    try:
        data = request.json
        if "request_type" in data and "asset_id" in data and "employee_id" in data and "description" in data:
            data = {
                "request_type": str(data['request_type']),
                "asset_id": int(data['asset_id']),
                "description": str(data['description']),
                "employee_id": int(data['employee_id'])
            }
            result = my.update("it_management", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
             return jsonify({"data":"request_type, asset_id, description, employee id", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/it_management/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.delete("it_management", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(debug=True, host="0.0.0.0", port=5000)
    # app.run(debug=True, port=1000)
    app.run(debug=True, host="0.0.0.0", port=5000)
