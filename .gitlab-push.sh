#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAqn3p2WxVS6q/Lq9eYpsxo9KquFM/fDqYPsHWnl1pns/mIBPk
DHYZAHK0voLM+DXhgAqQjr5K+hBFlWyrhM4EXo2dd3nhp4gqErO/KGUnjCHBVptq
UVEkHgBprKNWEC9mJzUxTibqPnlasXc/eIAn6eBouIEp9FGD4dnoW5riE77ZRr1+
roA7I3WjvRoioYHVtnxnk71xmF4vPxkK2S5F/FcZ1SAGD2E9waamKvum+oBmt1CX
6XL75OSEFO8imY7Ds7NKqVHp/uUkHdSeyPLxBVPhCE6gqtikjiL6TNMmUmLZbn41
JuXqSiTxSVNm/Piehi4Se8zwMEvhsyQ6EwPWGQIDAQABAoIBAAmbcMsEtMW/yGgs
D6iE0EZH/8DFSCWK87tNlvdzkhkV5ATYuhgNq5j/3bmCi2RNPPjHH6A7juQKeLFT
31JrfYmKd14dKx8SqDRHlrUDBjh6SnfD6DjO1rcuFMZaqeXdlecNs0qRHNAKbJbl
flke96DROXV0KdYPpdvbOKG415Z/KgoWpeiWKRsJLeD+W4gxnp3r5mQCvsm7nbFj
eTau/R85Iyo9JF/zQOlP6ib9gPCSqvTG3IRGW6ozRQ+FEe6l2YjwUHZkVcinV+lj
i4bsoHspXzRw/AEkDCWKWXroPmwtWazdIuicxFbaCn1oLGV3ltUFeW6LYFVxM8l2
ZPnXL1UCgYEA4HvMIBa5pw+mceYoTGQYurLAidLZ9l6nOi6ig2qCcY5d1CZSA/pK
O9ckdIgsEDUKVmRb4qMNY2WVgBucTEjZO7ANI35y+nQSnN0M7f72eMd+3EjUamXk
9R+lANDQUAdRgZm3vQThhB1e7V/7EwtB3Wa4HOxT/IvJslNqOSXnGZsCgYEAwm2X
C5mkfoFQ1GJDHPpMSAkJIwK7xlXmaYWsEmigVgs+W4/d1wcup8KpU+MGftm63LgJ
n1w9z1w8smYCQW9LY6K/6Y/idXhOFllJrArtvuTEAnksZ1bm+s+20NhLq35PrBwe
c9o5SzxYHm4d541Wy1cI0U36YZjlFtuW5arC9FsCgYAbrEcyoXqLdKtxGQ1YtXIy
+bwwgVSrQWneC0evaZ7g/srkT0EHCoDCNbhhwTOlX9SBZ+fH3Y/V8/zlCQJQiR52
dDJ6cKkd5NcyjTF92VNq+jTSVQwSfbMm4JrHqfY0F2Ld1iRHYeWnErPUEWPzKg0Z
jQMfVabY2CaqHAxZNSnzBwKBgQCQrr6k+juHjDGbKlgo+zXCsT6kWAdly0yAH+Hx
fPw7q3hG6KM7vXOBkYEyFJIj2WCNmUJH3Xt9kHT2iKSLDwf56AWKB/GMWu85AE6r
gSJ0WIZoFMXvTKLr56Byf1KoFoGxYOdkbPMPwqMMWgNU6T+6Z1T/dFirQfzQaq46
0DztWwKBgCReSW5Q97m9p5ne1iYM4+s/VV+aBHeQc0llvHBABXyZNeHCJKkfUqqH
efZPNsl4Y8Xxv+1+2ri4lYZeKDbkG1/DH7H0e+5cpIxfMGGsUFilArzKjzRq3xte
QkSroPWFXFVHmW15mWgxf6JmPCuYSeE/g5+MfEUlyTayKAV2MBm3
-----END RSA PRIVATE KEY-----" > key.pem
chmod 400 key.pem

if [ "$1" == "BUILD" ];then
# echo '[*] Building Program'
# echo "[*] Tag $WAKTU"
# for i in $(grep -iRl 'localhost' ./)
# do
# sed -i 's/localhost/172.17.0.1/g' $i
# done
# for i in $(ls | grep mc)
# do
# docker build -t isroalfajri/microservice:$i-$CI_COMMIT_BRANCH $i
# done
# docker login --username=$DOCKER_USER --password=$DOCKER_PASS
# for i in $(ls | grep mc)
# do
# docker push isroalfajri/microservice:$i-$CI_COMMIT_BRANCH
# done
# for i in $(ls | grep mc)
# do
# sed -e 's/MC-FOLDER/mc-pegawai/g' -e 's/BRANCH/master/g' -e 's/PORT/5001/g' kube.yaml
# done

docker login --username=$DOCKER_USER --password=$DOCKER_PASS

for i in $(ls | grep mc)
do
cp mysqlhelper.py $i
ls $i
docker build -t isroalfajri/microservice:$i-$CI_COMMIT_BRANCH $i
docker push isroalfajri/microservice:$i-$CI_COMMIT_BRANCH
done
fi