from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps
from mysqlhelper import *

app = Flask(__name__)

@app.route("/perjalanan_dinas", methods=['GET'])
def getbarang():
    my = MysqlHelper()
    try:
        result = my.get("perjalanan_dinas")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/perjalanan_dinas/<int:id>", methods=['GET'])
def getbarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.get_where("perjalanan_dinas", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/perjalanan_dinas", methods=['POST'])
def insertbarang():
    my = MysqlHelper()
    try:
        data = request.json
        if "dari" in data and "ke" in data and "ongkos" in data and "tujuan" in data:
            data = { 
                "dari": str(data['dari']),
                "ke": str(data['ke']),
                "ongkos": int(data['ongkos']),
                "tujuan": str(data['tujuan'])
            }
            result = my.insert("perjalanan_dinas", data)
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"dari, ke, ongkos, tujuan", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/perjalanan_dinas/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    my = MysqlHelper()
    try:
        data = request.json
        if "dari" in data and "ke" in data and "ongkos" in data and "tujuan" in data:
            data = {
                "dari": str(data['dari']),
                "ke": str(data['ke']),
                "ongkos": int(data['ongkos']),
                "tujuan": str(data['tujuan'])
            }
            result = my.update("perjalanan_dinas", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"dari, ke, ongkos, tujuan", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/perjalanan_dinas/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.delete("perjalanan_dinas", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    # app.run(host="0.0.0.0", port=5000)
    # app.run(debug=True, port=1000)
    app.run(debug=True, host="0.0.0.0", port=5000)