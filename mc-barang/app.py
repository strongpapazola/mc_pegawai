from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
from functools import wraps
from mysqlhelper import *

app = Flask(__name__)

@app.route("/barang", methods=['GET'])
def getbarang():
    my = MysqlHelper()
    try:
        result = my.get("barang")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang/<int:id>", methods=['GET'])
def getbarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.get_where("barang", {"id": id}, "row_array")
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang", methods=['POST'])
def insertbarang():
    my = MysqlHelper()
    try:
        data = request.json
        if "nama" in data and "jumlah" in data and "status" in data:
            data = {
                "nama": str(data['nama']),
                "jumlah": str(data['jumlah']),
                "status": str(data['status'])
            }
            result = my.insert("barang", data)
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"nama, jumlah, status", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    my = MysqlHelper()
    try:
        data = request.json
        if "nama" in data and "jumlah" in data and "status" in data:
            data = {
                "nama": str(data['nama']),
                "jumlah": int(data['jumlah']),
                "status": int(data['status'])
            }
            result = my.update("barang", data, {'id': id})
            if result:
                return jsonify({"data":result[1], "code": 200}), 200
            else:
                return jsonify({"data":result[1], "code": 500}), 500
        else:
            return jsonify({"data":"nama, jumlah, status", "code": 400}), 400
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    my = MysqlHelper()
    try:
        result = my.delete("barang", {"id": id})
        if result:
            return jsonify({"data":result[1], "code": 200}), 200
        else:
            return jsonify({"data":result[1], "code": 500}), 500
            
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    app.run(host="0.0.0.0", port=5000)
    # app.run(debug=True, port=1000)