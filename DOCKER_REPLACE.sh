#!/bin/bash
STATE=$(docker ps --all | grep 127.0.0.1:$1 | cut -d ' ' -f 1);
#STATE=$(docker ps --all | grep "mc_" | cut -d ' ' -f 1);
if [ "$STATE" != "" ];then docker stop $STATE; docker rm $STATE; else echo '[*] Not Detected Running Container'; fi
