import mysql.connector

def listToString(s):     
    str1 = ", ".join(map(str, s)) 
    return "({})".format(str1)

class MysqlHelper:
    def __init__(self):
        self.mydb = mysql.connector.connect(
            # host="localhost",
            host="117.53.45.42",
            user="root",
            passwd="@1Nfr4struktur",
            database="microservice_company"
        )

    def get(self, table, type="result_array"):
        c = self.mydb.cursor()
        c.execute("SELECT * FROM {}".format(table))
        myresult = c.fetchall()
        data = []
        for i in range(len(myresult)):
            field = [i[0] for i in c.description]
            row = {}
            for j in range(len(myresult[i])):
                row[field[j]] = myresult[i][j]
            data.append(row)
        if type == "result_array":
            return data
        elif type == "row_array":
            return data[0]

    def get_where(self, table, query={}, type="result_array"):
        c = self.mydb.cursor()
        if isinstance(query, str):
            where = query
        elif isinstance(query, dict):
            where = ""
            for i in query:
                where += "{} = '{}'".format(i, query[i],)
        # return where
        c.execute("SELECT * FROM {} WHERE {}".format(table, where))
        myresult = c.fetchall()
        data = []
        for i in range(len(myresult)):
            field = [i[0] for i in c.description]
            row = {}
            for j in range(len(myresult[i])):
                row[field[j]] = myresult[i][j]
            data.append(row)
        if type == "result_array":
            return data
        elif type == "row_array":
            return data[0]

    def insert(self, table, data={}):
        try:
            c = self.mydb.cursor()
            key = []
            val = []
            c.execute("SHOW KEYS FROM pegawai WHERE Key_name = 'PRIMARY'")
            primary = c.fetchone()[4]
            if primary not in data:
                key.append(primary)
                val.append("NULL")
            for i in data:
                key.append(i)
                val.append("'{}'".format(data[i]))
            key = listToString(key)
            val = listToString(val)
            c.execute("INSERT INTO {} {} VALUES {}".format(table, key, val))
            self.mydb.commit()
            return (True, "Record Inserted "+str(c.lastrowid))
        except Exception as e:
            return (False, 'SQL : '+str(e))

    def update(self, table, data={}, datawhere={}):
        try:
            c = self.mydb.cursor()
            val = []
            where = []
            for i in data:
                val.append("{} = '{}'".format(i, data[i], ))
            for i in datawhere:
                where.append("{} = '{}'".format(i, datawhere[i], ))
            val = ', '.join(val)
            where = ', '.join(where)

            sql = "UPDATE {} SET {} WHERE {}".format(table, val, where,)
            # return sql
            c.execute(sql)
            self.mydb.commit()
            return (True, "Record Affected")
        except Exception as e:
            return (False, 'SQL : '+str(e))

    def delete(self, table, datawhere={}):
        try:
            c = self.mydb.cursor()
            where = []
            for i in datawhere:
                where.append("{} = '{}'".format(i, datawhere[i],))
            where = ", ".join(where)
            sql = "DELETE FROM {} WHERE {}".format(table, where)
            c.execute(sql)
            self.mydb.commit()
            return (True, "{} record(s) deleted".format(c.rowcount))
        except Exception as e:
            return (False, 'SQL : '+str(e))


# o = MysqlHelper()
# print(o.get("pegawai"))
# print(o.get_where("pegawai", {"id": 1}))
# data = {
#         "nama": "bintang",
#         "email" : "bintang@gmail.com",
#         "npwp" : 2232432442022
#     }
# print(o.insert("pegawai", data))
# print(o.update("pegawai", data, {"id": 1}))
# print(o.delete("pegawai", {"id": 1}))
